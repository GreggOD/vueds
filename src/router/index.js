import Vue from "vue"
import Router from "vue-router"
import Index from "@/templates/Index"
import Home from "@/templates/Home"
import NotFound from "@/templates/NotFound"

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/",
      name: "Index",
      component: Index,
    },
    {
      path: "/home",
      name: "Home",
      component: Home,
    },
    {
      path: "*",
      name: "NotFound",
      component: NotFound,
    },
  ],
})
